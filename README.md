## provider-providerflclient

This provider controls federated learning clients parameter and exposes metrics from them to kubernetes API.

# parameters to select
```yaml
	ClientAddress: string "http://dtazzioli-edge.cloudmmwunibo.it:30001"
	BrokerAddress: string "as-sensiblecity1.cloudmmwunibo.it"
	ServerAddress: string "http://dtazzioli-cloud.cloudmmwunibo.it:3000/"
	BatchSize:     string "16"
	Epochs:        string "1"
	ClientID:      string "0"
	UpdatePeriod:  string "100.0"
	TopicName:     string "mqtttopic-1"
	Port:          string "3001"

```

# osservable metrics and parameters
```yaml
	Active:        string  {true or false}
	AverageLoss10: string  {loss of the last 10 epochs}
	AverageLoss5:  string  {loss of the last 5 epochs}
	Loss:          string  {loss of the last epoch}
	BatchSize:     string
	Epochs:        string
	ClientID:      string 
	UpdatePeriod:  string 
	TopicName:     string 
	BrokerAddress: string 
	ServerAddress: string 
	Port:          string 

```

# create the cluster externing the port for accessing the flclient
```shell
  kind create cluster --config=kind_config.yaml
```

# install crossplane
```shell
  helm install crossplane \
--namespace crossplane-system \
--create-namespace crossplane-stable/crossplane 
```

## provider to use kubernetes resources in crossplane composite resources
```shell
crossplane xpkg install provider xpkg.upbound.io/crossplane-contrib/provider-kubernetes:v0.13.0
```
```shell
SA=$(kubectl -n crossplane-system get sa -o name | grep provider-kubernetes | sed -e 's|serviceaccount\/|crossplane-system:|g')
kubectl create clusterrolebinding provider-kubernetes-admin-binding --clusterrole cluster-admin --serviceaccount="${SA}"
kubectl apply -f config-in-cluster.yaml
```

## installing crds and composite resource
```shell
kubectl apply -f package/crds/
#provider config
kubectl apply -f examples/provider/config.yaml
kubectl apply -f deploy_crossplane.yaml

#composite resource
kubectl apply -f examples/composite_resource/XRD_fl.yaml
kubectl apply -f examples/composite_resource/composition_fl.yaml
kubectl apply -f examples/composite_resource/xr_fl.yaml
```