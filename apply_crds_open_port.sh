#! bin/bash


#delete all childs
# kubectl get flclients -o json | kubectl delete -f -
kubectl delete -f examples/composite_resource/xr_fl.yaml



#crds and provider
kubectl apply -f package/crds/
#provider config
kubectl apply -f examples/provider/config.yaml
kubectl apply -f deploy_crossplane.yaml

#composite resource
kubectl apply -f examples/composite_resource/XRD_fl.yaml
kubectl apply -f examples/composite_resource/composition_fl.yaml
kubectl apply -f examples/composite_resource/xr_fl.yaml


#open port

kubectl proxy --port=8080 --address "0.0.0.0" --accept-hosts ".*"