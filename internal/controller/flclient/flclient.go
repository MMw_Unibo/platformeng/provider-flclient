/*
Copyright 2022 The Crossplane Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package flclient

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/pkg/errors"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/crossplane/crossplane-runtime/pkg/connection"
	"github.com/crossplane/crossplane-runtime/pkg/controller"
	"github.com/crossplane/crossplane-runtime/pkg/event"
	"github.com/crossplane/crossplane-runtime/pkg/logging"
	"github.com/crossplane/crossplane-runtime/pkg/ratelimiter"
	"github.com/crossplane/crossplane-runtime/pkg/reconciler/managed"
	"github.com/crossplane/crossplane-runtime/pkg/resource"

	xpv1 "github.com/crossplane/crossplane-runtime/apis/common/v1"
	"github.com/crossplane/provider-providerflclient/apis/federatedlearning/v1alpha1"
	apisv1alpha1 "github.com/crossplane/provider-providerflclient/apis/v1alpha1"
	"github.com/crossplane/provider-providerflclient/internal/features"
)

const (
	errNotFLClient  = "managed resource is not a FLClient custom resource"
	errTrackPCUsage = "cannot track ProviderConfig usage"
	errGetPC        = "cannot get ProviderConfig"
	errGetCreds     = "cannot get credentials"

	// errNewClient = "cannot create new Service"
)

// A NoOpService does nothing.
// type NoOpService struct{}

// var (
// 	newNoOpService = func(_ []byte) (interface{}, error) { return &NoOpService{}, nil }
// )

// Setup adds a controller that reconciles FLClient managed resources.
func Setup(mgr ctrl.Manager, o controller.Options) error {
	name := managed.ControllerName(v1alpha1.FLClientGroupKind)

	cps := []managed.ConnectionPublisher{managed.NewAPISecretPublisher(mgr.GetClient(), mgr.GetScheme())}
	if o.Features.Enabled(features.EnableAlphaExternalSecretStores) {
		cps = append(cps, connection.NewDetailsManager(mgr.GetClient(), apisv1alpha1.StoreConfigGroupVersionKind))
	}

	r := managed.NewReconciler(mgr,
		resource.ManagedKind(v1alpha1.FLClientGroupVersionKind),
		managed.WithExternalConnecter(&connector{
			kube:   mgr.GetClient(),
			usage:  resource.NewProviderConfigUsageTracker(mgr.GetClient(), &apisv1alpha1.ProviderConfigUsage{}),
			logger: o.Logger}),
		managed.WithLogger(o.Logger.WithValues("controller", name)),
		managed.WithPollInterval(o.PollInterval),
		managed.WithRecorder(event.NewAPIRecorder(mgr.GetEventRecorderFor(name))),
		managed.WithConnectionPublishers(cps...))

	return ctrl.NewControllerManagedBy(mgr).
		Named(name).
		WithOptions(o.ForControllerRuntime()).
		WithEventFilter(resource.DesiredStateChanged()).
		For(&v1alpha1.FLClient{}).
		Complete(ratelimiter.NewReconciler(name, r, o.GlobalRateLimiter))
}

// A connector is expected to produce an ExternalClient when its Connect method
// is called.
type connector struct {
	logger logging.Logger
	kube   client.Client
	usage  resource.Tracker
}

// Connect typically produces an ExternalClient by:
// 1. Tracking that the managed resource is using a ProviderConfig.
// 2. Getting the managed resource's ProviderConfig.
// 3. Getting the credentials specified by the ProviderConfig.
// 4. Using the credentials to form a client.
func (c *connector) Connect(ctx context.Context, mg resource.Managed) (managed.ExternalClient, error) {
	cr, ok := mg.(*v1alpha1.FLClient)
	if !ok {
		return nil, errors.New(errNotFLClient)
	}

	if err := c.usage.Track(ctx, mg); err != nil {
		return nil, errors.Wrap(err, errTrackPCUsage)
	}

	pc := &apisv1alpha1.ProviderConfig{}
	if err := c.kube.Get(ctx, types.NamespacedName{Name: cr.GetProviderConfigReference().Name}, pc); err != nil {
		return nil, errors.Wrap(err, errGetPC)
	}

	cd := pc.Spec.Credentials
	_, err := resource.CommonCredentialExtractor(ctx, cd.Source, c.kube, cd.CommonCredentialSelectors)
	if err != nil {
		return nil, errors.Wrap(err, errGetCreds)
	}

	// svc, err := c.newServiceFn(data)
	// if err != nil {
	// 	return nil, errors.Wrap(err, errNewClient)
	// }

	return &external{logger: c.logger}, nil
}

// An ExternalClient observes, then either creates, updates, or deletes an
// external resource to ensure it reflects the managed resource's desired state.
type external struct {
	// A 'client' used to connect to the external resource API. In practice this
	// would be something like an AWS SDK client
	// kube   client.Client
	logger logging.Logger
}

func (c *external) Observe(ctx context.Context, mg resource.Managed) (managed.ExternalObservation, error) {
	cr, ok := mg.(*v1alpha1.FLClient)
	if !ok {
		return managed.ExternalObservation{}, errors.New(errNotFLClient)
	}
	Uptodate := true

	metrics, err := c.ObserveClient(cr.Spec.ForProvider.ClientAddress)
	if err != nil {
		c.logger.Debug("OBSERVE: failed to observe the client")
		cr.Status.AtProvider = metrics
		// cr.Status.AtProvider.Active = "false"

	} else {
		c.logger.Debug("OBSERVE: the client is running")
		//copy metrics values into status.atprovider
		cr.Status.AtProvider = metrics
		// cr.Status.AtProvider.Active = "true"
	}

	//check for differences in state between two sctructures
	if cr.Spec.ForProvider.ClientID != cr.Status.AtProvider.ClientID {
		c.logger.Debug(fmt.Sprintf("ClientID: desired: %s, current: %s", cr.Spec.ForProvider.ClientID, cr.Status.AtProvider.ClientID))
		Uptodate = false
	}
	if cr.Spec.ForProvider.UpdatePeriod != cr.Status.AtProvider.UpdatePeriod {
		c.logger.Debug(fmt.Sprintf("UpdatePeriod: desired: %s, current: %s", cr.Spec.ForProvider.UpdatePeriod, cr.Status.AtProvider.UpdatePeriod))
		Uptodate = false
	}
	if cr.Spec.ForProvider.BatchSize != cr.Status.AtProvider.BatchSize {
		c.logger.Debug(fmt.Sprintf("BatchSize: desired: %s, current: %s", cr.Spec.ForProvider.BatchSize, cr.Status.AtProvider.BatchSize))
		Uptodate = false
	}
	if cr.Spec.ForProvider.Epochs != cr.Status.AtProvider.Epochs {
		c.logger.Debug(fmt.Sprintf("Epochs: desired: %s, current: %s", cr.Spec.ForProvider.Epochs, cr.Status.AtProvider.Epochs))
		Uptodate = false
	}
	if cr.Spec.ForProvider.TopicName != cr.Status.AtProvider.TopicName {
		c.logger.Debug(fmt.Sprintf("TopicName: desired: %s, current: %s", cr.Spec.ForProvider.TopicName, cr.Status.AtProvider.TopicName))
		Uptodate = false
	}
	if cr.Spec.ForProvider.BrokerAddress != cr.Status.AtProvider.BrokerAddress {
		c.logger.Debug(fmt.Sprintf("BrokerAddress: desired: %s, current: %s", cr.Spec.ForProvider.BrokerAddress, cr.Status.AtProvider.BrokerAddress))
		Uptodate = false
	}
	if cr.Spec.ForProvider.ServerAddress != cr.Status.AtProvider.ServerAddress {
		c.logger.Debug(fmt.Sprintf("ServerAddress: desired: %s, current: %s", cr.Spec.ForProvider.ServerAddress, cr.Status.AtProvider.ServerAddress))
		Uptodate = false
	}
	if cr.Spec.ForProvider.Port != cr.Status.AtProvider.Port {
		c.logger.Debug(fmt.Sprintf("Port: desired: %s, current: %s", cr.Spec.ForProvider.Port, cr.Status.AtProvider.Port))
		Uptodate = false
	}
	if Uptodate {
		c.logger.Debug("OBSERVE: the client is up to date")
		cr.SetConditions(xpv1.Available())
	} else {
		c.logger.Debug("OBSERVE: the client is not up to date")
		cr.SetConditions(xpv1.Unavailable())
	}

	resource_exists, err := strconv.ParseBool(cr.Status.AtProvider.Active)
	if err != nil {
		c.logger.Debug(fmt.Sprintf("Error in parsing active status: %s", err))
	}

	return managed.ExternalObservation{
		// Return false when the external resource does not exist. This lets
		// the managed resource reconciler know that it needs to call Create to
		// (re)create the resource, or that it has successfully been deleted.
		ResourceExists: resource_exists,

		// Return false when the external resource exists, but it not up to date
		// with the desired managed resource state. This lets the managed
		// resource reconciler know that it needs to call Update.
		ResourceUpToDate: Uptodate,

		// Return any details that may be required to connect to the external
		// resource. These will be stored as the connection secret.
		ConnectionDetails: managed.ConnectionDetails{},
	}, nil
}

func (c *external) Create(ctx context.Context, mg resource.Managed) (managed.ExternalCreation, error) {
	cr, ok := mg.(*v1alpha1.FLClient)
	if !ok {
		return managed.ExternalCreation{}, errors.New(errNotFLClient)
	}
	c.logger.Debug("Creating")
	//creating flclient on a pod in kubernetes
	cr.Status.AtProvider.Active = "true"

	return managed.ExternalCreation{
		// Optionally return any details that may be required to connect to the
		// external resource. These will be stored as the connection secret.
		ConnectionDetails: managed.ConnectionDetails{},
	}, nil
}

func (c *external) Update(ctx context.Context, mg resource.Managed) (managed.ExternalUpdate, error) {
	cr, ok := mg.(*v1alpha1.FLClient)
	if !ok {
		return managed.ExternalUpdate{}, errors.New(errNotFLClient)
	}

	c.logger.Debug("Updating")

	//transform cr.Spec.ForProvider into a json object

	data_to_send := map[string]string{}
	data_to_send["broker_address"] = cr.Spec.ForProvider.BrokerAddress
	data_to_send["server_address"] = cr.Spec.ForProvider.ServerAddress
	data_to_send["batch_size"] = cr.Spec.ForProvider.BatchSize
	data_to_send["epochs"] = cr.Spec.ForProvider.Epochs
	data_to_send["client_id"] = cr.Spec.ForProvider.ClientID
	data_to_send["update_period"] = cr.Spec.ForProvider.UpdatePeriod
	data_to_send["topic_name"] = cr.Spec.ForProvider.TopicName
	data_to_send["port"] = cr.Spec.ForProvider.Port

	data, err := json.Marshal(data_to_send)
	if err != nil {
		c.logger.Debug(fmt.Sprintf("Error in marshalling json: %s", err))

	}
	//sending data to the flclient

	resp, err := http.Post(cr.Spec.ForProvider.ClientAddress, "application/json", bytes.NewBuffer(data))
	if err != nil {
		c.logger.Debug(fmt.Sprintf("Error in sending data: %s", err))
	}

	defer resp.Body.Close()

	c.logger.Debug(fmt.Sprintf("Response status: %s", resp.Status))

	return managed.ExternalUpdate{
		// Optionally return any details that may be required to connect to the
		// external resource. These will be stored as the connection secret.
		ConnectionDetails: managed.ConnectionDetails{},
	}, nil
}

func (c *external) Delete(ctx context.Context, mg resource.Managed) error {
	cr, ok := mg.(*v1alpha1.FLClient)
	if !ok {
		return errors.New(errNotFLClient)
	}

	//we don't need to delete the server, it will be deleted by the user
	c.logger.Debug("Deleting")
	//simulated deletion
	cr.Status.AtProvider.Active = "false"

	return nil
}
