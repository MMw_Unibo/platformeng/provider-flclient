package flclient

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/crossplane/provider-providerflclient/apis/federatedlearning/v1alpha1"
)

func (c *external) ObserveClient(client_address string) (v1alpha1.FLClientObservation, error) {
	c.logger.Debug(fmt.Sprintf("Osserving flclient on %s", client_address))
	//make a get request on url:port
	resp, err := http.Get(client_address)
	if err != nil {
		c.logger.Debug(fmt.Sprintf("Get error: %s", err))
		return v1alpha1.FLClientObservation{}, err
	}
	defer resp.Body.Close()
	c.logger.Debug(fmt.Sprintf("Response status: %s", resp.Status))
	//read response with json
	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		c.logger.Debug(fmt.Sprintf("Error in decoding json: %s", err))
		return v1alpha1.FLClientObservation{}, err
	}
	c.logger.Debug(fmt.Sprintf("Decoded json: %s", data))

	var metrics v1alpha1.FLClientObservation
	metrics.Active = "true"
	metrics.AverageLoss10 = data["average_loss_10"].(string)
	metrics.AverageLoss5 = data["average_loss_5"].(string)
	metrics.Loss = data["loss"].(string)
	metrics.BatchSize = data["batch_size"].(string)
	metrics.Epochs = data["epochs"].(string)
	metrics.ClientID = data["client_id"].(string)
	metrics.UpdatePeriod = data["update_period"].(string)
	metrics.TopicName = data["topic_name"].(string)
	metrics.BrokerAddress = data["broker_address"].(string)
	metrics.ServerAddress = data["server_address"].(string)
	metrics.Port = data["port"].(string)
	return metrics, nil

}
