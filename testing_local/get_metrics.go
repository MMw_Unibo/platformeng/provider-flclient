package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func main() {
	url := "nodeserf.cloudmmwunibo.it"
	port := "3001"
	complete_url := fmt.Sprintf("%s:%s", url, port)
	fmt.Println(complete_url)
	//make a get request on url:port
	resp, err := http.Get(fmt.Sprintf("http://%s", complete_url))
	if err != nil {
		fmt.Println("Error in get request: ", err)
	}
	//read response with json
	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		fmt.Println("Error in decoding json: ", err)
	}
	fmt.Println(data)
	fmt.Println(data["average_loss_5"])
}
